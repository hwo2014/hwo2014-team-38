/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var _ = require('lodash');
var args = require('minimist')(process.argv.slice(2));
var App = require('./lib/App');

if ((args.h)||(args.help)) {
  console.log('Usage: [--arg val ...] [serverHost] [serverPort] [botName] [botKey]');
  console.log(' --botName name       Overrides any botName argument');
  console.log(' --botKey key         Overrides any botKey argument');
  console.log(' --serverHost name    Overrides any serverHost argument');
  console.log(' --serverPort port    Overrides any serverPort argument');
  console.log(' --track name         Set track name');
  console.log(' --console            Display important information at console and send other data into log files');
  console.log(' --AI mode');
  console.log('      The AI modes are as follows:');
  console.log('      race = Enable full racing mode');
  console.log('      tests = Execute all tests [ braking, acceleraion, cornering ] one per lap');
  console.log('      test:braking = Execute braking test');
  console.log('      test:acceleration = Execute acceleration test');
  console.log('      test:cornering = Execute cornering test');
  process.exit(0);
}

var serverHost = args._.length>0?args._[0]:null;
var serverPort = args._.length>1?args._[1]:null;
var botName = args._.length>2?args._[2]:null;
var botKey = args._.length>3?args._[3]:null;

var isError = false;

// Enable default argument overwrite from argument options
if (args.botName) {
  botName = args.botName;
}
if (args.botKey) {
  botKey = args.botKey;
}
if (args.serverHost) {
  serverHost = args.serverHost;
}
if (args.serverPort) {
  serverPort = args.serverPort;
}

if (!botName) {
  console.error('Error: botName argument is mandatory');
  process.exit(0);
}
if (!botKey) {
  console.error('Error: botKey argument is mandatory');
  process.exit(0);
}
if (!serverHost) {
  console.error('Error: serverHost argument is mandatory');
  process.exit(0);
}
if (!serverPort) {
  console.error('Error: serverPort argument is mandatory');
  process.exit(0);
}

var app = new App({
  serverHost: serverHost,
  serverPort: serverPort,
  botName: botName,
  botKey: botKey,
  args: args
}, {
});
