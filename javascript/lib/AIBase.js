/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var Moment = require('moment');

var Track = require('./Track');
var Simulation = require('./Simulation');
var Player = require('./Player');
var Helpers = require('./Helpers');

var helpers = new Helpers();

/**
 * AIBase is the Base class for AI classes
 * @param {app} app Handle to the app class
 * @param {ext} ext Handle to the extensions
 */
function AIBase(app, ext) {
  this.app = app;
  this.ext = ext;

  // Create objects
  this.ext.simulation = new Simulation(this);
  this.ext.track = new Track();

  this.initialize();
}

AIBase.prototype.initialize = function() {
  return false;
};

AIBase.prototype.loadTrackProfile = function(options) {
  if (fs.existsSync('profiles')) {
    var profileName = options.profileName?options.profileName:this.ext.track.id;
    var fileName = path.join('profiles', profileName + '.json');
    if (fs.existsSync(fileName)) {
      this.profile = JSON.parse(fs.readFileSync(fileName)); //require(fileName);
      console.log('Track profile "' + profileName + '" loaded.');
    } else {
      console.log('Track profile "' + profileName + '" not found. Creating new');
      var now = new Moment();
      this.profile = {
        track: {
          id: this.ext.track.id,
          name: this.ext.track.name
        },
        created: now.format("YYYY-MM-DDTHH:mm:ssZ"),
        modified: null,
        acceleration: [],
        braking: []
      };
    }
  }
};

AIBase.prototype.saveTrackProfile = function(options) {
  if (fs.existsSync('profiles')) {
    var now = new Moment();
    var profileName = options.profileName?options.profileName:this.ext.track.id;
    var fileName = path.join('profiles', profileName + '.json');
    this.profile.modified = now.format("YYYY-MM-DDTHH:mm:ssZ");
    fs.writeFileSync(fileName, JSON.stringify(this.profile, null, 4), 'utf8');
  }
};

/*
 *                           8
 *  eeeee eeeee eeeeeee eeee 8  eeeee e eeeee
 *  8   8 8   8 8  8  8 8    8e 8   8 8   8
 *  8e    8eee8 8e 8  8 8eee 88 8e  8 8e  8e
 *  88 "8 88  8 88 8  8 88   88 88  8 88  88
 *  88ee8 88  8 88 8  8 88ee 88 88  8 88  88
 *
 *  Message = {
 *    "msgType": "gameInit",
 *    "data": {
 *      "race": {
 *         "track": {
 *           "id": "indianapolis",
 *           "name": "Indianapolis",
 *           "pieces": [
 *             {
 *               "length": 100.0
 *             },
 *             {
 *               "length": 100.0,
 *               "switch": true
 *             },
 *             {
 *               "radius": 200,
 *               "angle": 22.5
 *             }
 *           ],
 *           "lanes": [
 *             {
 *               "distanceFromCenter": -20,
 *               "index": 0
 *             },
 *             {
 *               "distanceFromCenter": 0,
 *               "index": 1
 *             },
 *             {
 *               "distanceFromCenter": 20,
 *               "index": 2
 *             }
 *           ],
 *           "startingPoint": {
 *             "position": {
 *               "x": -340.0,
 *               "y": -96.0
 *             },
 *             "angle": 90.0
 *           }
 *         },
 *         "cars": [
 *           {
 *             "id": {
 *               "name": "Schumacher",
 *               "color": "red"
 *             },
 *             "dimensions": {
 *               "length": 40.0,
 *               "width": 20.0,
 *               "guideFlagPosition": 10.0
 *             }
 *           },
 *           {
 *             "id": {
 *               "name": "Rosberg",
 *               "color": "blue"
 *             },
 *             "dimensions": {
 *               "length": 40.0,
 *               "width": 20.0,
 *               "guideFlagPosition": 10.0
 *             }
 *           }
 *         ],
 *         "raceSession": {
 *           "laps": 3,
 *           "maxLapTimeMs": 30000,
 *           "quickRace": true
 *        }
 *      }
 *    }
 *  }
 */
AIBase.prototype.onGameInit = function(msg) {

  console.log('[AIBase] onGameInit');
  console.log(msg.data);

  var _this = this;
  var race = msg.data.race;

  // Setup Track Data
  var track = race.track;
  var pieces = track.pieces;
  var lanes = track.lanes;
  var startingPoint = track.startingPoint;

  this.ext.track.setup(track.id, track.name, startingPoint.angle);
  this.ext.track.setLanes(lanes);
  this.ext.track.setPieces(pieces);

  this.loadTrackProfile({
    profileName: this.app.args.profileName?this.app.args.profileName:null
  });

  this.saveTrackProfile({
    profileName: this.app.args.profileName?this.app.args.profileName:null
  });

  console.log('Track = ' + track.name + ' (' + track.id + ') with ' + lanes.length + ' lane(s)');

  _.forEach(this.ext.track.lanes, function(lane){
    console.log('Track Lane #' + lane.index + ' = ' + lane.totalLength.toFixed(2) + ' unit(s) long');
  });

  // Setup Player(s) Data
  var cars = race.cars;
  this.players = {};
  _.forEach(cars, function(car){
    var id = car.id.color + ':' + car.id.name;
    _this.players[id] = new Player({
      id: id
    });
    console.log('Player #' + _.keys(_this.players).length + ' = ' + car.id.name + ' (' + car.id.color + ')');
  });

  // Setup Session Data
  console.log('Race Session =', race.raceSession);

  // Setup Simulation
  this.ext.simulation.setup(this.myId, this.ext.track, this.players, startingPoint.angle);

  // Reset used variables
  this.LANE_SWITCH_PENDING = null;

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0/*, gameTick: msg.gameTick*/ });
};

/*
 *                    8""""8
 *  eeee eeeee eeeee  8    8 eeeee eeeee e eeeee e  eeeee eeeee eeeee
 *  8  8 8   8 8   8  8eeee8 8  88 8   " 8   8   8  8  88 8   8 8   "
 *  8e   8eee8 8eee8e 88     8   8 8eeee 8e  8e  8e 8   8 8e  8 8eeee
 *  88   88  8 88   8 88     8   8    88 88  88  88 8   8 88  8    88
 *  88e8 88  8 88   8 88     8eee8 8ee88 88  88  88 8eee8 88  8 8ee88
 *
 *  Message = {
 *    "msgType": "carPositions",
 *    "data": [
 *      {
 *        "id": {
 *          "name": "Schumacher",
 *          "color": "red"
 *        },
 *        "angle": 0.0,
 *        "piecePosition": {
 *          "pieceIndex": 0,
 *          "inPieceDistance": 0.0,
 *          "lane": {
 *            "startLaneIndex": 0,
 *            "endLaneIndex": 0
 *          },
 *          "lap": 0
 *        }
 *      },
 *      {
 *        "id": {
 *          "name": "Rosberg",
 *          "color": "blue"
 *        },
 *        "angle": 45.0,
 *        "piecePosition": {
 *          "pieceIndex": 0,
 *          "inPieceDistance": 20.0,
 *          "lane": {
 *            "startLaneIndex": 1,
 *            "endLaneIndex": 1
 *          },
 *          "lap": 0
 *        }
 *      }
 *    ],
 *    "gameId": "OIUHGERJWEOI",
 *    "gameTick": 0
 *  }
 */
AIBase.prototype.onCarPositions = function(msg) {
  this.ext.simulation.update(msg);
  this.me = this.ext.simulation.players[this.myId];
};

/*
 *     e  eeeee e  eeeee eeee eeeee
 *     8  8  88 8  8   8 8    8   8
 *     8e 8   8 8e 8e  8 8eee 8e  8
 *  e  88 8   8 88 88  8 88   88  8
 *  8ee88 8eee8 88 88  8 88ee 88ee8
 */
AIBase.prototype.onJoin = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onJoin');

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0/*, gameTick: msg.gameTick*/ });
};

/*
 *                            8""""8
 *  e    e eeeee e   e eeeee  8    " eeeee eeeee
 *  8    8 8  88 8   8 8   8  8e     8   8 8   8
 *  8eeee8 8   8 8e  8 8eee8e 88     8eee8 8eee8e
 *    88   8   8 88  8 88   8 88   e 88  8 88   8
 *    88   8eee8 88ee8 88   8 88eee8 88  8 88   8
 */
AIBase.prototype.onYourCar = function(msg) {

  this.name = msg.data.name;
  this.color = msg.data.color;
  this.gameId = msg.data.gameId;
  this.myId = this.color + ':' + this.name;

  helpers.clearStatusLine();
  console.log('[AIBase] onYourCar. Registered at race as ' + this.name + ' (color:' + this.color + ')');

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0/*, gameTick: msg.gameTick*/ });
};

/*
 *                           8""""8
 *  eeeee eeeee eeeeeee eeee 8      eeeee eeeee eeeee eeeee
 *  8   8 8   8 8  8  8 8    8eeeee   8   8   8 8   8   8
 *  8e    8eee8 8e 8  8 8eee     88   8e  8eee8 8eee8e  8e
 *  88 "8 88  8 88 8  8 88   e   88   88  88  8 88   8  88
 *  88ee8 88  8 88 8  8 88ee 8eee88   88  88  8 88   8  88
 */
AIBase.prototype.onGameStart = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onGameStart');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0/*, gameTick: msg.gameTick*/ });
};

/*
 *  eeee eeeee  eeeee eeeee e   e
 *  8  8 8   8  8   8 8   " 8   8
 *  8e   8eee8e 8eee8 8eeee 8eee8
 *  88   88   8 88  8    88 88  8
 *  88e8 88   8 88  8 8ee88 88  8
 */
AIBase.prototype.onCrash = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onCrash');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping", gameTick: msg.gameTick });
};

/*
 *  eeeee eeeee eeeee e   e  e eeeee
 *  8   " 8   8 8   8 8   8  8 8   8
 *  8eeee 8eee8 8eee8 8e  8  8 8e  8
 *     88 88    88  8 88  8  8 88  8
 *  8ee88 88    88  8 88ee8ee8 88  8
 */
AIBase.prototype.onSpawn = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onSpawn');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0, gameTick: msg.gameTick });
};

/*
 *                    8""""
 *  e     eeeee eeeee 8     e  eeeee e  eeeee e   e eeee eeeee
 *  8     8   8 8   8 8eeee 8  8   8 8  8   " 8   8 8    8   8
 *  8e    8eee8 8eee8 88    8e 8e  8 8e 8eeee 8eee8 8eee 8e  8
 *  88    88  8 88    88    88 88  8 88    88 88  8 88   88  8
 *  88eee 88  8 88    88    88 88  8 88 8ee88 88  8 88ee 88ee8
 */
AIBase.prototype.onLapFinished = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onLapFinished. Lap Time = '+ helpers.millisToTime(msg.data.lapTime.millis));
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send full throttle to server
  this.app.sendMsg({ msgType: "throttle", data: 1.0 });
};

/*
 *  eeeee eeeee eeee
 *  8   8 8   8 8
 *  8e  8 8e  8 8eee
 *  88  8 88  8 88
 *  88ee8 88  8 88
 */
AIBase.prototype.onDNF = function(msg) {
  helpers.clearStatusLine();
  console.log('[AIBase] onDNF');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping", gameTick: msg.gameTick });
};

/*
 *  eeee e  eeeee e  eeeee e   e
 *  8    8  8   8 8  8   " 8   8
 *  8eee 8e 8e  8 8e 8eeee 8eee8
 *  88   88 88  8 88    88 88  8
 *  88   88 88  8 88 8ee88 88  8
 */
AIBase.prototype.onFinish = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onFinish');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping", gameTick: msg.gameTick });
};

/*
 *                           8""""
 *  eeeee eeeee eeeeeee eeee 8     eeeee eeeee
 *  8   8 8   8 8  8  8 8    8eeee 8   8 8   8
 *  8e    8eee8 8e 8  8 8eee 88    8e  8 8e  8
 *  88 "8 88  8 88 8  8 88   88    88  8 88  8
 *  88ee8 88  8 88 8  8 88ee 88eee 88  8 88ee8
 */
AIBase.prototype.onGameEnd = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onGameEnd');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping"/*, gameTick: msg.gameTick*/ });
};

/*
 *                                                                8""""
 *  eeeee eeeee e   e eeeee  eeeee eeeee eeeeeee eeee eeeee eeeee 8     eeeee eeeee
 *    8   8  88 8   8 8   8  8   8 8   8 8  8  8 8    8   8   8   8eeee 8   8 8   8
 *    8e  8   8 8e  8 8eee8e 8e  8 8eee8 8e 8  8 8eee 8e  8   8e  88    8e  8 8e  8
 *    88  8   8 88  8 88   8 88  8 88  8 88 8  8 88   88  8   88  88    88  8 88  8
 *    88  8eee8 88ee8 88   8 88  8 88  8 88 8  8 88ee 88  8   88  88eee 88  8 88ee8
 */
AIBase.prototype.onTournamentEnd = function(msg) {

  helpers.clearStatusLine();
  console.log('[AIBase] onTournamentEnd');
  console.log('--------------------------------------');
  console.log(msg);
  console.log('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping"/*, gameTick: msg.gameTick*/ });
};

/*
 *                                 8""""8
 * eeeee e   e eeeee  eeeee  eeeee 8    8 ee   e eeeee e  e     eeeee eeeee  e     eeee
 *   8   8   8 8   8  8   8  8  88 8eeee8 88   8 8   8 8  8     8   8 8   8  8     8
 *   8e  8e  8 8eee8e 8eee8e 8   8 88   8 88  e8 8eee8 8e 8e    8eee8 8eee8e 8e    8eee
 *   88  88  8 88   8 88   8 8   8 88   8  8  8  88  8 88 88    88  8 88   8 88    88
 *   88  88ee8 88   8 88eee8 8eee8 88   8  8ee8  88  8 88 88eee 88  8 88eee8 88eee 88ee
 */
AIBase.prototype.onTurboAvailable = function(msg) {
  helpers.clearStatusLine();
  console.warn('[AIBase] onTurboAvailable');
  console.warn('--------------------------------------');
  console.warn(msg);
  console.warn('======================================');
};

/*
 *  e   e eeeee e   e  eeeee eeeee e   e  e eeeee
 *  8   8 8   8 8   8  8   8 8  88 8   8  8 8   8
 *  8e  8 8e  8 8eee8e 8e  8 8   8 8e  8  8 8e  8
 *  88  8 88  8 88   8 88  8 8   8 88  8  8 88  8
 *  88ee8 88  8 88   8 88  8 8eee8 88ee8ee8 88  8
 */
AIBase.prototype.onUnHandledMessage = function(msg) {

  helpers.clearStatusLine();
  console.warn('[AIBase] onUnHandledMessage');
  console.warn('--------------------------------------');
  console.warn(msg);
  console.warn('======================================');

  // Send ping to server
  this.app.sendMsg({ msgType: "ping" });
};

module.exports = AIBase;
