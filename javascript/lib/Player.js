var _ = require('lodash');

function Player(options) {
  this.id = options.id;
  this.latest = null;
  this.previous = null;
  this.previous2 = null;
  this.pos = 0;
  this.angle = null;
}

Player.prototype.update = function(data) {
  if (this.previous) {
    this.previous2 = _.clone(this.previous, true);
  } else {
    this.previous2 = _.clone(data, true);
  }
  if (this.latest) {
    this.previous = _.clone(this.latest, true);
  } else {
    this.previous = _.clone(data, true);
  }
  this.latest = _.clone(data, true);
  this.latest.controls = {
    set: false,
    throttle: null,
    laneChange: null,
    targetSpeed: null
  };
};

Player.prototype.setTargetSpeed = function(targetSpeed) {
  this.latest.controls.targetSpeed = targetSpeed;
};

Player.prototype.setThrottle = function(throttle) {
  if (!this.latest.controls.set) {
    if (throttle < 0.0) { throttle = 0.0; }
    if (throttle > 1.0) { throttle = 1.0; }
    this.latest.controls.throttle = throttle;
    this.latest.controls.set = true;
  }
};

Player.prototype.setSwitchLane = function(switchLane) {
  if (!this.latest.controls.set) {
    this.latest.controls.switchLane = switchLane;
    this.latest.controls.set = true;
  }
};

module.exports = Player;
