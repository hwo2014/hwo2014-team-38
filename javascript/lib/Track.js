/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var _ = require('lodash');
var Helpers = require('./Helpers');
var helpers = new Helpers();

function Track() {
}

Track.prototype.reset = function() {
  this.id = undefined;
  this.name = undefined;
  this.laneCount = undefined;
  this.pieces = [];
  this.lanes = [];
};

Track.prototype.setup = function(id, name, startAngle) {
  this.reset();
  this.id = id;
  this.name = name;
  this.startAngle = startAngle;
  this.lastAngle = this.startAngle;
};

Track.prototype.getLength = function() {
  return 0;
};

Track.prototype.setLanes = function(lanes) {
  this.lanes = _.clone(lanes, true);
  this.laneCount = this.lanes.length;
};

Track.prototype.calculateTotalLengths = function() {
  var _this = this;
  _.forEach(this.lanes, function(lane){
    var len = 0;
    _.forEach(_this.pieces, function(piece){
      len += piece.lengths[lane.index];
    });
    lane.totalLength = len;
  });
};

Track.prototype.setPieces = function(pieces) {
  this.pieces = [];
  this.addPieces(pieces);
  this.generateSegments();
  this.calculateTotalLengths();
};

Track.prototype.generateSegments = function() {
  var _this = this;
  // Given array of track pieces we will generate
  // track segments and link segments together
  // as a last step (meaning that the last segment
  // could be welded to the first segment)
  // - Need to have links between pieces to segments
  //   with correct start position information
  this.segments = [];

  // -----------------------------------------------------------
  // Mark different parts of track with segment indexes
  // Note: Effectively joins curves and lines together
  // -----------------------------------------------------------
  var segmentIndex = 0;
  var pieceIndex = 0;
  var angle = 0, radius = 0;
  var lastAngle = null;
  var lastRadius = null;
  var firstPiece = this.pieces[0];
  var lastSegment = _.clone(this.pieces[0], true);
  var startPositions = [];
  var i;
  for (i=0; i!=firstPiece.lengths.length; ++i) {
    startPositions.push(0);
  }
  lastSegment.pieces = [];
  _.forEach(this.pieces, function(piece){
    if (_.has(piece, 'angle')) {
      angle = piece.angle;
    } else {
      angle = 0;
    }
    radius = piece.radius;
    if (angle!==lastAngle || lastAngle===null || radius!==lastRadius || lastRadius===null) {
      if (lastAngle!==null && lastRadius!==null) {
        _this.segments.push(lastSegment);
        lastSegment = _.clone(piece, true);
        lastSegment.pieces = [];
        segmentIndex++;
      }
      lastAngle = angle;
      lastRadius = radius;
      // reset start positions
      for (i=0; i!=piece.lengths.length; ++i) {
        startPositions[i] = 0;
      }
    } else {
      // add to existing segment
      if (_.has(piece, 'switch') && piece.switch===true) {
        lastSegment.switch = true;
      }
      if (_.has(piece, 'angle')) {
        lastSegment.angle += angle;
      }
      for (i=0; i!=piece.lengths.length; ++i) {
        lastSegment.lengths[i] += piece.lengths[i];
      }
    }
    piece.segmentIndex = segmentIndex;
    lastSegment.pieces.push(pieceIndex);
    lastSegment.index = segmentIndex;
    piece.segmentPositions = _.clone(startPositions, true);
    for (i=0; i!=piece.lengths.length; ++i) {
      startPositions[i] += piece.lengths[i];
    }
    pieceIndex++;
  });
  this.segments.push(lastSegment);

  // if last and first pieces are at the same angle
  // they must be directly connected so modify last
  // piece index to match and
  if (_.has(this.pieces[0], 'angle')) {
    angle = this.pieces[0].angle;
  } else {
    angle = 0;
  }
  if (angle===lastAngle) {
    for (i=0; i!=this.pieces[0].lengths.length; ++i) {
      startPositions[i] = 0;
    }
    var pieces = [];
    pieceIndex = this.pieces.length-1;
    _.forEachRight(this.pieces, function(piece){
      if (piece.segmentIndex===segmentIndex) {
        piece.segmentIndex = 0;
        for (i=0; i!=piece.lengths.length; ++i) {
          startPositions[i] += piece.lengths[i];
        }
        pieces.push(pieceIndex);
        pieceIndex--;
        return true;
      } else {
        return false;
      }
    });
    _.forEach(this.segments, function(segment){
      if (segment.index===0) {
        for (i=0; i!=segment.lengths.length; ++i) {
          segment.lengths[i] += startPositions[i];
        }
        _.forEach(pieces, function(piece){
          segment.pieces.push(piece);
        });
        segment.pieces = segment.pieces.sort();
        return true;
      } else {
        return false;
      }
    });
    _.forEach(this.pieces, function(piece){
      if (piece.segmentIndex===0) {
        for (i=0; i!=piece.lengths.length; ++i) {
          piece.segmentPositions[i] += startPositions[i];
        }
        return true;
      } else {
        return false;
      }
    });
    // remove last segment
    this.segments.pop();
  }
  /*
  console.log('------------- Pieces --------------');
  _.forEach(this.pieces, function(piece){
    console.log(piece);
  });
  console.log('------------- Segments --------------');
  _.forEach(this.segments, function(segment){
    console.log(segment);
  });
  */
};

Track.prototype.addPiece = function(piece) {
  this.pieces.push(piece);
  if (_.has(piece, 'angle')) {
    this.lastAngle += piece.angle;
    if (this.lastAngle < 0) {
      this.lastAngle += 360;
    } else if (this.lastAngle > 359) {
      this.lastAngle -= 360;
    }
  }
};

Track.prototype.addLinePiece = function(len, hasSwitch) {
  var lengths = [];
  for (var i=0; i!=this.laneCount; ++i) {
    lengths.push(len);
  }
  var mirror = this.lastAngle > 180;
  this.addPiece({
    "heading": this.lastAngle,
    "mirror": mirror,
    "lengths": lengths,
    "switch": hasSwitch===true
  });
};

Track.prototype.addCurvePiece = function(radius, angle, hasSwitch) {
  var lengths = [];
  /*
  // calculate lengths for all lanes
  var mirror = this.lastAngle > 180;
  if (helpers.almostEqual(this.lastAngle,180)) {
    // we are going down, check which side we are turning
    if (angle<0) {
      mirror = false;
    } else {
      mirror = true;
    }
  }
  */
  var mirror = angle<0;
  var len;
  for (var i=0; i!=this.laneCount; ++i) {
    if (mirror) {
      len = parseFloat((radius+this.lanes[i].distanceFromCenter)*2)*Math.PI*(Math.abs(parseFloat(Math.abs(angle)))/360.0);
    } else {
      len = parseFloat((radius-this.lanes[i].distanceFromCenter)*2)*Math.PI*(Math.abs(parseFloat(Math.abs(angle)))/360.0);
    }
    lengths.push(len);
  }
  this.addPiece({
    "heading": this.lastAngle,
    "mirror": mirror,
    "lengths": lengths,
    "radius": radius,
    "angle": angle,
    "switch": hasSwitch===true
  });
};

Track.prototype.addPieces = function(pieces) {
  var _this = this;
  _.forEach(pieces, function(piece){
    if (piece.length) {
      _this.addLinePiece(piece.length, piece.switch);
    } else if (piece.radius) {
      _this.addCurvePiece(piece.radius, piece.angle, piece.switch);
    }
  });
};

module.exports = Track;
