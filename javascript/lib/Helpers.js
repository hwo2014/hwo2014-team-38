/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var moment = require('moment');

var _SS = '                                                                                                    ';
var _SL = 100;
var _ZZ = '0000000000';
var _ZL = 10;
var LastStatusLineLen;
var FLT_EPSILON = 1.19209290e-7;
var DBL_EPSILON = 2.2204460492503131e-16;

function Helpers() {
}

Helpers.prototype.writeStatusLine = function(t) {
  var fillUp = LastStatusLineLen?LastStatusLineLen-t.length:0;
  process.stdout.write('\r' + t + this.rpad('', fillUp));
  LastStatusLineLen = t.length;
};

Helpers.prototype.clearStatusLine = function() {
  var fillUp = LastStatusLineLen?LastStatusLineLen:0;
  process.stdout.write('\r' + this.rpad('', fillUp) + '\r');
  LastStatusLineLen = 0;
};

Helpers.prototype.almostEqual = function(a, b) {
  var d = Math.abs(a - b);
  if(d <= FLT_EPSILON) {
    return true;
  }
  if(d <= FLT_EPSILON * Math.min(Math.abs(a), Math.abs(b))) {
    return true;
  }
  return a === b;
};

Helpers.prototype.almostEqualDouble = function(a, b) {
  var d = Math.abs(a - b);
  if(d <= DBL_EPSILON) {
    return true;
  }
  if(d <= DBL_EPSILON * Math.min(Math.abs(a), Math.abs(b))) {
    return true;
  }
  return a === b;
};

Helpers.prototype.lpad = function(t, len) {
  return _SS.substring(_SL-(len-t.length))+t;
};

Helpers.prototype.rpad = function(t, len) {
  return t+_SS.substring(_SL-(len-t.length));
};

Helpers.prototype.lfzpad = function(num, fdlen, len, sign) {
  var t, tsign;
  if (typeof num === 'number') {
    if (num<0) {
      tsign = '-';
      len--;
      t = Math.abs(num).toFixed(fdlen);
    } else {
      if (sign) {
        tsign = '+';
        len--;
      } else {
        tsign = '';
      }
      t = num.toFixed(fdlen);
    }
  } else {
    tsign = '!';
    len--;
    t = '';
  }
  return tsign + _ZZ.substring(_ZL-(len-t.length))+t;
};

Helpers.prototype.lzpad = function(num, len, sign) {
  var t, tsign;
  if (typeof num === 'number') {
    if (num<0) {
      tsign = '-';
      len--;
      t = Math.abs(num).toString();
    } else {
      if (sign) {
        tsign = '+';
        len--;
      } else {
        tsign = '';
      }
      t = num.toString();
    }
  } else {
    tsign = '!';
    len--;
    t = '';
  }
  return tsign + _ZZ.substring(_ZL-(len-t.length))+t;
};

Helpers.prototype.millisToTime = function(millis) {
  var duration = moment.duration(parseInt(millis, 10));
  var addZero = function(v) { return (v<10 ? '0' : '') + v.toFixed(0); };
  return addZero(duration.minutes()) +
    ':' + addZero(duration.seconds()) +
    '.' + addZero(duration.milliseconds()/10);
};

Helpers.prototype.peekArray = function(arr) {
 if (arr.length!==0) { return arr[arr.length-1]; }
 return null;
};

module.exports = Helpers;
