/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var net = require("net");
var JSONStream = require('JSONStream');
var _ = require('lodash');
var util = require('util');
var moment = require('moment');
var log4js = require('log4js');
var Helpers = require('./Helpers');

var helpers = new Helpers();
var now = moment();
var timestamp = 'latest'; //now.format('YYYYMMDDTHHmmss')

// The Main Application entry point
function App(opts, ext) {
  this.ext = ext;
  this.args = opts.args;
  this.log4js = log4js;

  if (this.args.console) {
    // Configure log4js
    if (this.args.log) {
      timestamp = this.args.log;
    }
    log4js.configure({
      appenders: [
        { type: 'console', category: 'console' },
        { type: 'file', filename: 'logs/' + timestamp + '_data.txt', category: 'data', layout: { type: 'messagePassThrough' }, absolute: true },
        { type: 'file', filename: 'logs/' + timestamp + '_stats.txt', category: 'stats', layout: { type: 'messagePassThrough' }, absolute: true },
        { type: 'file', filename: 'logs/' + timestamp + '_console.txt', category: 'console', absolute: true }
      ],
      replaceConsole: true
    });
  } else {
    // Configure log4js
    log4js.configure({
      appenders: [
        { type: 'console', category: 'console' },
        { type: 'console', category: 'data' },
        { type: 'console', category: 'stats' }
      ],
      replaceConsole: true
    });
  }
  var _this = this;

  this.serverHost = opts.serverHost;
  this.serverPort = opts.serverPort;
  this.botName = opts.botName;
  this.botKey = opts.botKey;

  // Use default base for now
  var AIFileName = this.args.AI?this.args.AI:'race';
  var AIClass = require('../AI/AI_' + AIFileName);
  this.AI = new AIClass(this, this.ext);

  // Set logging levels
  this.log = {
    console: log4js.getLogger('console'),
    stats: log4js.getLogger('stats'),
    data: log4js.getLogger('data')
  };
  this.log.console.setLevel('ALL');
  this.log.stats.setLevel('ALL');
  this.log.data.setLevel('ALL');

  // Handle server messages
  this.fmap = {
    'join': _this.AI.onJoin,
    'joinrace': _this.AI.onJoin,
    'yourcar': _this.AI.onYourCar,
    'gameinit': _this.AI.onGameInit,
    'gamestart': _this.AI.onGameStart,
    'carpositions': _this.AI.onCarPositions,
    'crash': _this.AI.onCrash,
    'spawn': _this.AI.onSpawn,
    'lapfinished': _this.AI.onLapFinished,
    'dnf': _this.AI.onDNF,
    'finish': _this.AI.onFinish,
    'gameend': _this.AI.onGameEnd,
    'tournamentend': _this.AI.onTournamentEnd,
    'turboavailable': _this.AI.onTurboAvailable,
    'unhandledmessage': _this.AI.onUnHandledMessage
  };

  // Open initial connection to the server
  this.connect();
}

App.prototype.disconnect = function() {
  var _this = this;
  this.client.end();
  this.client.destroy();
};

App.prototype.exit = function() {
  var _this = this;
  this.client.end();
  this.client.destroy();
  log4js.shutdown(function() { process.exit(0); });
};

/**
 * Connect again to the server restarting the session
 */
App.prototype.reconnect = function() {
  var _this = this;
  this.client.end();
  this.client.destroy();
  this.connect();
};

/**
 * Connect to server
 */
App.prototype.connect = function() {
  var _this = this;

  // Create connection to server
  this.log.console.info(this.botName, 'connecting to', this.serverHost + ':' + this.serverPort);
  this.client = net.connect(this.serverPort, this.serverHost, function() {

    _this.jsonStream = _this.client.pipe(JSONStream.parse());

    // We have connection, send join message to start racing
    if (!_this.args.track) {
      _this.sendMsg({
        msgType: "join",
        data: {
          name: _this.botName,
          key: _this.botKey
        }
      });
    } else {
      _this.sendMsg({
        msgType: "joinRace",
        data: {
          botId: {
            name: _this.botName,
            key: _this.botKey,
          },
          trackName: _this.args.track,
          "carCount": _this.args.cars?_this.args.cars:1
        }
      });
    }

    _this.jsonStream.on('data', function(data) {
      if (_this.args.console) {
        _this.log.data.info(util.inspect(data, { depth: null, colors:false}));
      } else {
        if (data.msgType!=='carPositions') {
          _this.log.data.info(util.inspect(data, { depth: null, colors:false}));
        }
      }
      var msgType = data.msgType.toLowerCase();
      var f;
      if (_.has(_this.fmap, msgType)) {
        f = _this.fmap[msgType];
        f.call(_this.AI, data);
      } else {
        f = _this.fmap['unhandledmessage'];
        f.call(_this.AI, data);
      }
    });

    _this.jsonStream.on('error', function(err) {
      console.log('** jsonStream error', err);
      console.log("disconnected");
      return;
    });
  });
};

App.prototype.sendMsg = function(msg) {
  this.client.write(JSON.stringify(msg));
  this.log.data.info(util.inspect(msg, { depth: null, colors:false}));
  return this.client.write('\n');
};

module.exports = App;
