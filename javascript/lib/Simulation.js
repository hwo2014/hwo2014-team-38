/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var _ = require('lodash');
var util = require('util');
var Helpers = require('./Helpers');
var helpers = new Helpers();

function Simulation(app) {
  this.App = app;
}

Simulation.prototype.setup = function(playerId, track, players, angle) {
  this.playerId = playerId;
  this.track = track;
  this.players = players;
  this.gameTick = undefined;
  this.startingAngle = angle;
  this.state = {
    laneChange: {
      pending: false,
      direction: null
    },
    throttle: 0.0,
    turbo: {
      count: 0,
      inprogress: false,
      ticksLeft: 0
    }
  };
};

Simulation.prototype.getNthSegment = function(index, nth, lane) {
  var segment, distance = 0;
  var i=0;
  do {
    if (segment) { distance += segment.lengths[lane]; }
    index++;
    if (index>=this.track.segments.length-1) {
      index = 0;
    }
    segment = this.track.segments[index];
    i++;
  } while (i<nth);
  return { segment: segment, index: index, distance: distance };
};

Simulation.prototype.getNthPiece = function(index, nth, lane) {
  var piece, distance = 0;
  var i=0;
  do {
    if (piece) { distance += piece.lengths[lane]; }
    index++;
    if (index>=this.track.pieces.length-1) {
      index = 0;
    }
    piece = this.track.pieces[index];
    i++;
  } while (i<nth);
  return { piece: piece, index: index, distance: distance };
};

Simulation.prototype.findNextLinePiece = function(index, lane) {
  // loop until we have found next curve piece
  var piece;
  var distance = 0;
  do {
    if (piece) { distance += piece.lengths[lane]; }
    index++;
    if (index>=this.track.pieces.length-1) {
      index = 0;
    }
    piece = this.track.pieces[index];
  } while (_.has(piece, 'angle'));
  return { piece: piece, index: index, distance: distance };
};

Simulation.prototype.findNextCurvePiece = function(index, lane) {
  // loop until we have found next curve piece
  var piece;
  var distance = 0;
  do {
    if (piece) { distance += piece.lengths[lane]; }
    index++;
    if (index>=this.track.pieces.length-1) {
      index = 0;
    }
    piece = this.track.pieces[index];
  } while (!_.has(piece, 'angle'));
  return { piece: piece, index: index, distance: distance };
};

Simulation.prototype.findNextSwitchPiece = function(index, lane) {
  // loop until we have found next switch piece
  var piece;
  var distance = 0;
  var distances = [];
  var lanes = this.track.lanes.length;
  function addToDistances(v,k){
    distances[k] = distances[k] || 0;
    distances[k]+=v;
  }
  function addToDistancesMirror(v,k){
    distances[lanes-k-1] = distances[lanes-k-1] || 0;
    distances[lanes-k-1]+=v;
  }
  do {
    if (piece) {
      var lastHeading = this.track.pieces[index].heading;
      /*
      var mirror = lastHeading > 180;
      if (helpers.almostEqual(lastHeading,180)) {
        // we are going down, check which side we are turning
        if (piece.angle<0) {
          mirror = false;
        } else {
          mirror = true;
        }
      }
      //console.log('findNextSwitchPiece - Adding piece #'+index+', lengths=', piece.lengths, 'mirror=',mirror);
      if (mirror) {
        distance += piece.lengths[this.track.lanes.length-lane-1];
        _.forEach(piece.lengths, addToDistancesMirror, this);
      } else {
        distance += piece.lengths[lane];
        _.forEach(piece.lengths, addToDistances, this);
      }*/
      distance += piece.lengths[lane];
      _.forEach(piece.lengths, addToDistances, this);
    }
    index++;
    if (index>=this.track.pieces.length-1) {
      index = 0;
    }
    piece = this.track.pieces[index];
  } while (!(_.has(piece, 'switch') && piece.switch===true));
  //console.log('- Distances =', distances);
  return { piece: piece, index: index, distance: distance, distances: distances };
};

Simulation.prototype.findNextLineSegment = function(index, lane) {
  // loop until we have found next curve segment
  var segment;
  var distance = 0;
  do {
    if (segment) { distance += segment.lengths[lane]; }
    index++;
    if (index>=this.track.segments.length-1) {
      index = 0;
    }
    segment = this.track.segments[index];
  } while (_.has(segment, 'angle'));
  return { segment: segment, index: index, distance: distance };
};

Simulation.prototype.findNextCurveSegment = function(index, lane) {
  // loop until we have found next curve segment
  var segment;
  var distance = 0;
  do {
    if (segment) { distance += segment.lengths[lane]; }
    index++;
    if (index>=this.track.segments.length-1) {
      index = 0;
    }
    segment = this.track.segments[index];
  } while (!_.has(segment, 'angle'));
  return { segment: segment, index: index, distance: distance };
};

Simulation.prototype.update = function(msg) {

  var _this = this;

  this.prevTick = this.gameTick?this.gameTick:msg.gameTick-1;
  this.gameTick = msg.gameTick;

  _.forEach(msg.data, function(car){
    var id = car.id.color + ':' + car.id.name;
    var player = _this.players[id];
    player.update(car);
  });

  var dt = this.gameTick - this.prevTick;

  // calculate player diffs
  _.forEach(this.players, function(player){

    player.angle = player.angle || _this.startingAngle;

    var prevSpeed = player.previous.speed;
    var prevAngle = player.previous.angle;
    var prevPieceIndex = player.previous.piecePosition.pieceIndex;
    var prevLane = player.previous.piecePosition.lane.startLaneIndex;
    var prevLaneEnd = player.previous.piecePosition.lane.endLaneIndex;
    var prevPiecePos = player.previous.piecePosition.inPieceDistance;
    var prevPiece = _this.track.pieces[prevPieceIndex];
    var prevPieceLen;
    if (prevPiece.angle<0) {
      prevPieceLen = parseFloat(prevPiece.lengths[_this.track.lanes.length-prevLane-1]);
    } else {
      prevPieceLen = parseFloat(prevPiece.lengths[prevLane]);
    }
    if (prevLane!==prevLaneEnd) {
      if (_.has(prevPiece, 'angle')) {
        var l1 = prevPiece.lengths[prevLane];
        var l2 = prevPiece.lengths[prevLaneEnd];
        var r = prevPiece.radius;
        var d = (parseFloat(l2)-parseFloat(l1));
        if (d<0) {
          prevPieceLen -= 1.413;
        } else {
          prevPieceLen += 1.413;
        }
      } else {
        // for 100 length piece it is exactly
        prevPieceLen += 2.06;
      }
    }
    var curAngle = player.latest.angle;
    var curPieceIndex = player.latest.piecePosition.pieceIndex;
    var curLane = player.latest.piecePosition.lane.startLaneIndex;
    var curLaneEnd = player.latest.piecePosition.lane.endLaneIndex;
    var curLanes = _this.track.lanes.length;
    var curPiecePos = player.latest.piecePosition.inPieceDistance;
    var curPiece = _this.track.pieces[curPieceIndex];
    var curPieceLen;
    if (curPiece.angle<0) {
      curPieceLen = parseFloat(curPiece.lengths[_this.track.lanes.length-curLane-1]);
    } else {
      curPieceLen = parseFloat(curPiece.lengths[curLane]);
    }
    /*
    if (curLane!==curLaneEnd) {
      // We are changing lanes in this piece
      // add dummy 2.00 to lane length
      curPieceLen += 2.06;
    }
    */
    var curPieceLeft = curPieceLen - curPiecePos;

    var next = _this.getNthPiece(curPieceIndex, 1, curLane);
    var nextPiece = next.piece;
    var nextPieceIndex = next.index;
    var nextPieceDistance = next.distance + curPieceLeft;

    var nextCurve = _this.findNextCurvePiece(curPieceIndex, curLane);
    var nextCurvePiece = nextCurve.piece;
    var nextCurveDistance = nextCurve.distance + curPieceLeft;

    var nextLine = _this.findNextLinePiece(curPieceIndex, curLane);
    var nextLinePiece = nextLine.piece;
    var nextLineDistance = nextLine.distance + curPieceLeft;

    var nextSwitch = _this.findNextSwitchPiece(curPieceIndex, curLane);
    var nextSwitchPiece = nextSwitch.piece;
    var nextSwitchIndex = nextSwitch.index;
    var nextSwitchDistance = nextSwitch.distance;
    var nextSwitchDistances = nextSwitch.distances;

    var next2ndSwitch = _this.findNextSwitchPiece(nextSwitch.index, curLane);
    var next2ndSwitchPiece = next2ndSwitch.piece;
    var next2ndSwitchIndex = next2ndSwitch.index;
    var next2ndSwitchDistance = next2ndSwitch.distance;
    var next2ndSwitchDistances = next2ndSwitch.distances;

    var curSegmentIndex = curPiece.segmentIndex;
    var curSegment = _this.track.segments[curSegmentIndex];

    var nextSegmentInfo = _this.getNthSegment(curSegmentIndex, 1, curLane);
    var nextSegment = nextSegmentInfo.segment;
    var nextSegmentDistance = nextSegmentInfo.distance;

    var nextCurveSegmentInfo = _this.findNextCurveSegment(curSegmentIndex, curLane);
    var nextCurveSegment = nextCurveSegmentInfo.segment;
    var nextCurveSegmentDistance = nextCurveSegmentInfo.distance;

    var curSegmentLength = curSegment.lengths[curLane];
    var curSegmentPos = curPiece.segmentPositions[curLane] + curPiecePos;
    var curSegmentLeft = curSegmentLength - curSegmentPos;

    var speed, acceleration, anglespeed;

    anglespeed = curAngle - prevAngle;
    anglespeedabs = Math.abs(prevAngle - curAngle);

    if (prevPieceIndex === curPieceIndex) {
      speed = (curPiecePos - prevPiecePos)/dt;
      /*
      console.log('-------------------------');
      console.log('Same start and end pieces');
      console.log('-------------------------');
      console.log('Piece:', curPiece);
      console.log('PrevPos:', prevPiecePos);
      console.log('CurPos:', );
      console.log('Speed:', curPiecePos);
      */
    } else {
      // set the current angle depending on the last piece
      player.angle += prevPiece.angle?prevPiece.angle:0;
      if (player.angle < 0) {
        player.angle += 360;
      } else if (player.angle > 359) {
        player.angle -= 360;
      }
      var fromPiece = prevPieceLen - prevPiecePos;
      speed = player.previous.speed + player.previous.acceleration; //(fromPiece + curPiecePos) / dt;



      /*
      console.log('------------------------------');
      console.log('Different start and end pieces');
      console.log('------------------------------');
      console.log('Start piece:', prevPiece, prevPieceIndex);
      console.log('End piece:', curPiece, curPieceIndex);
      console.log('PrevPos:', prevPiecePos);
      console.log('CurPos:', curPiecePos);
      console.log('Speed:', speed);
      */
    }

    /*
    if (curLane!==prevLane) {
      // we have just changed lanes, indicate that with speed which is calculated from last two speeds
      speed = player.speed2; // + (player.speed2-player.speed3);
      // also do not move car forward at all, fake the speed only
    } else {
      player.pos += speed;
    }
    */

    if (isNaN(speed)) {
      speed = 0.0;
    }

    acceleration = speed - prevSpeed;

    if (isNaN(acceleration)) {
      acceleration = 0.0;
    }

    player.speed = speed;
    player.latest.acceleration = acceleration;
    player.latest.speed = speed;

    player.curSegment = curSegment;
    player.curSegmentIndex = curSegmentIndex;
    player.curSegmentPos = curSegmentPos;
    player.curSegmentLeft = curSegmentLeft;

    player.nextCurve = nextCurvePiece;
    player.nextCurveDistance = nextCurveDistance;

    player.nextLine = nextLinePiece;
    player.nextLineDistance = nextLineDistance;

    player.nextSegment = nextSegment;
    player.nextSegmentDistance = nextSegmentDistance + curSegmentLeft;

    player.nextCurveSegment = nextCurveSegment;
    player.nextCurveSegmentDistance = nextCurveSegmentDistance + curSegmentLeft;

    player.curPiece = curPiece;
    player.curPieceIndex = curPieceIndex;
    player.curPiecePos = curPiecePos;
    player.curPieceLeft = curPieceLeft;

    player.curLane = curLane;
    player.curLanes = curLanes;

    player.nextPiece = nextPiece;
    player.nextPieceDistance = nextPieceDistance;
    player.nextPieceIndex = nextPieceIndex;

    player.nextSwitch = nextSwitchPiece;
    player.nextSwitchDistance = nextSwitchDistance;
    player.nextSwitchDistances = nextSwitchDistances;
    player.nextSwitchIndex = nextSwitchIndex;

    player.next2ndSwitch = next2ndSwitchPiece;
    player.next2ndSwitchDistance = next2ndSwitchDistance;
    player.next2ndSwitchDistances = next2ndSwitchDistances;
    player.next2ndSwitchIndex = next2ndSwitchIndex;

    player.acceleration = acceleration;
    player.anglespeed = anglespeed;
  });

};

Simulation.prototype.simulate = function() {
  var _this = this;
  var dt = 0;
  var now = new Date().valueOf();
  if (this.now) {
    dt = now-this.now;
    this.gameTick += dt;
  } else {
    this.gameTick = 0;
  }
  this.now = now;
  // advance cars
  _.forEach(this.players, function(player){
    var curPieceIndex = player.piecePosition.pieceIndex;
    var curLane = player.piecePosition.lane.startLaneIndex;
    var curPiece = _this.track.pieces[curPieceIndex];
    var maxPieces = _this.track.pieces.length;
    var pieceLen = parseFloat(curPiece.lengths[curLane]);
    var curPiecePos = player.piecePosition.inPieceDistance;
    var carSpeed = player.state.throttle * dt; //(parseFloat(dt)/10);
    if (carSpeed < 1.0) {
      carSpeed = 1.0;
    }
    curPiecePos += carSpeed;
    if (curPiecePos > pieceLen) {
      // jump to next piece
      curPiecePos -= pieceLen;
      curPiecePos = parseFloat(curPiecePos);
      curPieceIndex++;
      if (curPieceIndex >= maxPieces) {
        curPieceIndex = 0;
        player.piecePosition.lap++;
      }
      player.piecePosition.pieceIndex = curPieceIndex;

    }
    player.piecePosition.inPieceDistance = curPiecePos;
  });

  return {
    "msgType": "carPositions",
    "data": this.players,
    "gameTick": this.gameTick
  };
};

module.exports = Simulation;
