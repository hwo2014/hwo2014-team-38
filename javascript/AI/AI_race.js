/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Mika "Fincodr" Luoma-aho
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var _ = require('lodash');
var util = require('util');
var Helpers = require('../lib/Helpers');
var AIBase = require('../lib/AIBase');

var helpers = new Helpers();

function AI() {
  // Call the parent constructor
  AIBase.apply(this, arguments);
}

util.inherits(AI, AIBase);

AI.prototype.getMaxEnterSpeedForCurveSegment = function(curveSegment, curLane) {

  var r = curveSegment.radius;
  var a = curveSegment.angle;
  var l = curveSegment.lengths[curLane];
  if (r < 50) {
    return 3.0;
  } else if (r < 75) {
    return 3.5;
  } else if (r < 100) {
    return 4;
  } else if (r < 150) {
    return 6.5;
  }
  return 7.5;
};

AI.prototype.getBrakingDistanceForSpeed = function(currentSpeed, wantedSpeed) {
  if (currentSpeed<=wantedSpeed) {
    return 0;
  }
  return 50*(3*((currentSpeed/wantedSpeed)/3));
};

AI.prototype.getThrottleForSpeed = function(speed) {
  // Note: Currently fixed calculation
  var t = speed/10;
  if (t<0.0) {
    t=0.0;
  }
  if (t>1.0) {
    t=1.0;
  }
  return t;
};

AI.prototype.getMaxSpeedForCurveSegment = function(curveSegment, curLane, curPos) {
  return this.getMaxEnterSpeedForCurveSegment(curveSegment, curLane);
};

/**
 *                    8""""8
 *  eeee eeeee eeeee  8    8 eeeee eeeee e eeeee e  eeeee eeeee eeeee
 *  8  8 8   8 8   8  8eeee8 8  88 8   " 8   8   8  8  88 8   8 8   "
 *  8e   8eee8 8eee8e 88     8   8 8eeee 8e  8e  8e 8   8 8e  8 8eeee
 *  88   88  8 88   8 88     8   8    88 88  88  88 8   8 88  8    88
 *  88e8 88  8 88   8 88     8eee8 8ee88 88  88  88 8eee8 88  8 8ee88
 *
 *  AI - RACE
 *
 */
AI.prototype.onCarPositions = function(msg) {
  // Always call super
  AI.super_.prototype.onCarPositions.apply(this, arguments);

  var me = this.me;

  if (_.has(me, 'speed')) {

    var AIState = 'Idle';
    var msgSent = false;

    var segmentType = helpers.rpad(_.has(me.curSegment, 'angle')?'Curve'+helpers.lfzpad(me.curSegment.radius, 1, 4, false)+helpers.lfzpad(me.curSegment.angle, 1, 6, true):'Line',20);

    /***** Temporal lane change logic here ******/

    if (this.LANE_SWITCH_PENDING===null && me.curPieceLeft < 32) {
      // We do not yet have lane switch pending (note: it can't be cancelled)
      if (_.has(me.nextPiece, 'switch') && me.nextPiece.switch===true) {
        /*console.log('SWITCH PIECE IS NEXT | Mirror =', mirror);
        console.log('Next 1stSwitch @'+me.nextSwitchIndex+' =', me.nextSwitch);
        console.log('Next 2ndSwitch @'+me.next2ndSwitchIndex+' =', me.next2ndSwitch);*/
        var curDist = me.next2ndSwitchDistances[me.curLane];
        //console.log('Our lane #'+me.curLane+' has distance to next 2nd switch: ' + curDist);
        // Calculate which lane is the fastest way to the next switch piece
        var minDist = Number.MAX_VALUE;
        var bestLane = 0;
        for (var laneIndex=0; laneIndex!=me.next2ndSwitchDistances.length; ++laneIndex) {
          var distance = me.next2ndSwitchDistances[laneIndex];
          //console.log('Lane #'+laneIndex+' has distance to next 2nd switch: '+distance);
          if (distance<minDist) {
            minDist = distance;
            bestLane = laneIndex;
          }
        }
        helpers.clearStatusLine();
        if (curDist!=minDist) {
          if (me.curLane<bestLane) {
            this.app.sendMsg({"msgType": "switchLane", "data": "Right", gameTick: msg.gameTick });
            msgSent = true;
            AIState = 'SWITCHING_LANE_RIGHT';
          } else if (me.curLane>bestLane) {
            this.app.sendMsg({"msgType": "switchLane", "data": "Left", gameTick: msg.gameTick });
            msgSent = true;
            AIState = 'SWITCHING_LANE_LEFT';
          }
        }
        this.LANE_SWITCH_PENDING = me.nextPieceIndex;
      }
    } else {
      // Wait until we are at the switch piece
      if (me.curPieceIndex===this.LANE_SWITCH_PENDING) {
        this.LANE_SWITCH_PENDING = null;
      }
    }

    /***** ------------------------------- ******/

    if (!msgSent) {

      var WANTED_SPEED = 10;

      AIState = 'FULL_SPEED';

      var NextCurveMaxEnterSpeed = this.getMaxEnterSpeedForCurveSegment(me.nextCurveSegment, me.curLane);
      var NextCurveBrakingDistance = this.getBrakingDistanceForSpeed(me.speed, NextCurveMaxEnterSpeed);

      if (_.has(me.curSegment, 'angle')) {
        WANTED_SPEED = this.getMaxSpeedForCurveSegment(me.curSegment, me.curLane, me.curSegmentPos);
        AIState = 'CURVE_SPEED';
      }

      if (me.nextCurveSegmentDistance<NextCurveBrakingDistance) {
        WANTED_SPEED = Math.min(NextCurveMaxEnterSpeed, WANTED_SPEED);
        AIState = 'BRAKING_FOR_NEXT_CURVE';
      }

      var throttle = 0.0;
      if (me.speed>WANTED_SPEED) {
        throttle = 0.0;
      } else if (me.speed<WANTED_SPEED-1) {
        throttle = 1.0;
      } else {
        throttle = WANTED_SPEED/10;
      }

      this.app.sendMsg({ msgType: "throttle", data: throttle, gameTick: msg.gameTick });
    }

    // Update simulation state on status line
    if (this.app.args.console) {
      helpers.writeStatusLine(
        helpers.lzpad(this.ext.simulation.gameTick, 5) +  /* gameTick */
        ' | @' +
        helpers.lzpad(me.curSegmentIndex, 2) +
        '[' + helpers.lzpad(me.curPieceIndex, 2) + ']' +
        ':' +
        helpers.rpad(segmentType, 2) +
        ' | ' +
        helpers.lfzpad(me.curSegmentPos, 2, 5) +
        ' | ' +
        helpers.lfzpad(me.latest.controls.throttle, 2, 5) +
        ' | ' +
        helpers.lfzpad(me.latest.speed, 2, 5) +
        ' \u0394 ' +
        helpers.lfzpad(me.acceleration, 3, 3, true) +
        ' \u2220 ' +
        helpers.lfzpad(me.latest.angle, 2, 6, true) +
        ' \u0394 ' +
        helpers.lfzpad(me.anglespeed, 2, 6, true) +
        ' | NC ' +
        helpers.lfzpad(me.nextCurveSegmentDistance, 1, 6, true) +
        ' | ' +
        AIState
      );
      process.stdout.write('\n');
    }

    var angle = 0.00;
    var radius = 0.00;
    if (_.has(me.curPiece,'angle')) {
      angle = me.curPiece.angle;
    }
    if (_.has(me.curPiece,'radius')) {
      radius = me.curPiece.radius;
    }
    // Write statistics
    this.app.log.stats.info(
      this.ext.simulation.gameTick +
      '\t' +
      me.curPieceIndex +
      '\t' +
      segmentType +
      '\t' +
      me.curSegmentPos.toFixed(2).replace('.', ',') +
      '\t' +
      angle.toFixed(2).replace('.', ',') +
      '\t' +
      radius.toFixed(2).replace('.', ',') +
      '\t' +
      me.acceleration.toString().replace('.', ',') +
      '\t' +
      me.angle.toString().replace('.', ',') +
      '\t' +
      me.anglespeed.toString().replace('.', ',') +
      '\t' +
      me.nextCurveSegmentDistance.toString().replace('.', ',') +
      '\t' +
      AIState
    );

  } else {
    // Send constant throttle
    this.app.sendMsg({ msgType: "throttle", data: 1.0, gameTick: msg.gameTick });
  }
};

module.exports = AI;
